#+Title: snek: bootleg snake game made in SkeletonGL
#+Author: AlexHG @ xenspace.net


[[http://www.gnu.org/licenses/gpl-3.0.html][http://img.shields.io/:license-mit-blue.svg]]

  - Features: [[https://xenspace.net/projects?nav=snek][https://xenspace.net/projects?nav=snek]]

* About SNEK
  A simple snake game made as an example application for the [[https://gitlab.com/xenspace/skeletongl][skeletongl]] game engine.

* Installation
1. Install the dependencies. For Debian / Ubuntu :
#+BEGIN_SRC 
sudo apt-get update
sudo apt-get install libsdl2-dev libglew-dev libfreetype6-dev
#+END_SRC

2. Clone the repo
#+BEGIN_SRC 
git clone https://gitlab.com/xenspace/snek 
#+END_SRC

3. Compile and play. 
#+BEGIN_SRC 
cd snek
make -j$(nproc)
./snek 
#+END_SRC


