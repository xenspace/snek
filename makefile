#           _______  _        _______  _______  _______  _______  _______     _        _______ _________
# |\     /|(  ____ \( (    /|(  ____ \(  ____ )(  ___  )(  ____ \(  ____ \   ( (    /|(  ____ \\__   __/
# ( \   / )| (    \/|  \  ( || (    \/| (    )|| (   ) || (    \/| (    \/   |  \  ( || (    \/   ) (
#  \ (_) / | (__    |   \ | || (_____ | (____)|| (___) || |      | (__       |   \ | || (__       | |
#   ) _ (  |  __)   | (\ \) |(_____  )|  _____)|  ___  || |      |  __)      | (\ \) ||  __)      | |
#  / ( ) \ | (      | | \   |      ) || (      | (   ) || |      | (         | | \   || (         | |
# ( /   \ )| (____/\| )  \  |/\____) || )      | )   ( || (____/\| (____/\ _ | )  \  || (____/\   | |
# |/     \|(_______/|/    )_)\_______)|/       |/     \|(_______/(_______/(_)|/    )_)(_______/   )_(
# Author: AlexHG @ xenspace.net
# License: MIT. Use at your own risk.

CC=clang

LDFLAGS= -g -Wall -fPIC -m64 -lm -lstdc++ -std=c++14 $(shell pkg-config --cflags --libs sdl2)  $(shell pkg-config --cflags --libs freetype2) $(shell pkg-config glew --cflags --libs) -pthread -lglut 

# SkeletonGL core files
SOURCES= src/main.cpp \
		 src/player.cpp \
		 src/gameManager.cpp \
		 src/gameObject.cpp \
		 src/grid.cpp \
	 	 src/skeletonGL/window/SGL_Window.cpp \
		 src/skeletonGL/utility/SGL_AssetManager.cpp \
		 src/skeletonGL/utility/SGL_Utility.cpp \
		 src/skeletonGL/renderer/SGL_OpenGLManager.cpp \
		 src/skeletonGL/renderer/SGL_Shader.cpp \
		 src/skeletonGL/renderer/SGL_Texture.cpp \
		 src/skeletonGL/renderer/SGL_PostProcessor.cpp \
		 src/skeletonGL/renderer/SGL_Camera.cpp \
		 src/skeletonGL/renderer/SGL_Renderer.cpp


OBJECTS=$(SOURCES:.cpp=.o)

EXECUTABLE=snake_sgl

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC)  $(OBJECTS) $(LDFLAGS) -o $@

.cpp.o:
	$(CC) $< $(LDFLAGS) -c -o $@


clean:
	rm -rf src/*.o
	rm -rf src/skeletonGL/*.o
	rm -rf src/skeletonGL/renderer/*.o
	rm -rf src/skeletonGL/window/*.o
	rm -rf src/skeletonGL/utility/*.o
