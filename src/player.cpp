//           _______  _        _______  _______  _______  _______  _______     _        _______ _________
// |\     /|(  ____ \( (    /|(  ____ \(  ____ )(  ___  )(  ____ \(  ____ \   ( (    /|(  ____ \\__   __/
// ( \   / )| (    \/|  \  ( || (    \/| (    )|| (   ) || (    \/| (    \/   |  \  ( || (    \/   ) (
//  \ (_) / | (__    |   \ | || (_____ | (____)|| (___) || |      | (__       |   \ | || (__       | |
//   ) _ (  |  __)   | (\ \) |(_____  )|  _____)|  ___  || |      |  __)      | (\ \) ||  __)      | |
//  / ( ) \ | (      | | \   |      ) || (      | (   ) || |      | (         | | \   || (         | |
// ( /   \ )| (____/\| )  \  |/\____) || )      | )   ( || (____/\| (____/\ _ | )  \  || (____/\   | |
// |/     \|(_______/|/    )_)\_______)|/       |/     \|(_______/(_______/(_)|/    )_)(_______/   )_(
// Author: AlexHG @ xenspace.net
// License: MIT. Use at your own risk.

#include "player.hpp"
#include <cmath>
#include <math.h>

Player::Player()
{
    this->proRenderHitbox = true;
}

void Player::init(SGL_Texture texture)
{
    this->proPhysics.acceleration = {2.0f, 2.0f};

    //temprary UVs
    //this->proSprite->changeUVCoords(0, 0, 64, 64);

    this->proSprite->texture = texture;
    this->proSprite->size = glm::vec2(16, 16);
    this->proSprite->position = glm::vec2(0, 0);
    this->proSprite->rotationOrigin = glm::vec2(32, 32);
    this->proSprite->blending = BLENDING_TYPE::DEFAULT_RENDERING;
    this->proSprite->color = SGL_Color(1.0f, 1.0f, 1.0f, 1.0f);
    //proSprite->changeUVCoords(0, 0, 320, 320);
    this->proSprite->resetUVCoords();
    //hitbox poisition
    this->proHitbox.x = 20;
    this->proHitbox.y = 20;

    rot = 0.0f;
    //default hitbox sprite properties
    proHitboxSprite->color = SGL_Color(1.0,1.0,1.0,1.0);
    proHitboxSprite->position = glm::vec2(100,100);
    proHitboxSprite->size = glm::vec2(64, 64);
    proHitboxSprite->resetUVCoords();
    //proHitboxSprite->changeUVCoords(0, 0, 16, 16);


}

Player::~Player()
{

}


void Player::update(GLfloat deltaTime)
{
    //move the hitbox
    //this->physics.velocity.x += this->physics.acceleration.x * deltaTime;
    //this->physics.velocity.y += this->physics.acceleration.y * deltaTime;

    //this->hitbox.x += static_cast<int>(this->physics.velocity.x);
    //this->hitbox.y += static_cast<int>(this->physics.velocity.y);

    //this->hitbox.x += static_cast<int>(this->physics.velocity.x * deltaTime);
    // this->proHitbox.x += (this->proPhysics.velocity.x);
    // this->proHitbox.y += (this->proPhysics.velocity.y);

    //move the sprite
    this->proSprite->position = glm::vec2(this->proHitbox.x, this->proHitbox.y);
    this->proHitboxSprite->position = glm::vec2(this->proHitbox.x, this->proHitbox.y);
    //this->hitbox.y += static_cast<int>(this->physics.velocity.y * deltaTime);

    //rotate the sprite so it faces the cursor
    // pLog->print("Player posX: ", this->proSprite->position.x);
    float deltaX = this->pMouseDeltaX - (this->proSprite->rotationOrigin.x + this->proSprite->position.x);
    float deltaY = this->pMouseDeltaY - (this->proSprite->rotationOrigin.y + this->proSprite->position.y);
    // pLog->print("X: ", input.cameraMousePos.x);
    // pLog->print("Y: ", input.cameraMousePos.y);
    float angle = std::atan2(deltaY, deltaX);

    this->proSprite->rotation = angle;
    // pLog->print("Player angle: ", angle);
    //Log::out("rot: ", angle);
    // std::string rotationStr = "rot: " + std::to_string(angle);
    // Log::print(LOG_LEVEL::DEBUG, rotationStr);
    //particles
    int newParticles = 1;
    if (this->proPhysics.velocity.x == 0 && this->proPhysics.velocity.y == 0)
        newParticles = 0;

    // this->pParticles->update(deltaTime, glm::vec2(this->proSprite->position.x + 25, this->proSprite->position.y + 25), glm::vec2(15.0f, 15.0f), newParticles, 16.0f, glm::vec2(0,0), 8);

}

void Player::render()
{
    // pLog->print("player texture ID ", proSprite->texture.ID);
    // pLog->print("player taxutre w ", proSprite->texture.width);
    // pParticles->draw();
}

void Player::input(SGL_InputFrame &state)
{
    //x axis
    if (state.left)
    {
        this->proPhysics.velocity.x = -this->proPhysics.acceleration.x;
    }
    if (state.right)
    {
        this->proPhysics.velocity.x = this->proPhysics.acceleration.x;
    }

    if (!state.left && !state.right)
        this->proPhysics.velocity.x = 0;

    //y axis
    if (state.up)
        this->proPhysics.velocity.y = -this->proPhysics.acceleration.y;
    if (state.down)
        this->proPhysics.velocity.y = this->proPhysics.acceleration.y;

    if (!state.down && !state.up)
        this->proPhysics.velocity.y = 0;


    this->pMouseDeltaX = state.normalizedMousePosX;
    this->pMouseDeltaY = state.normalizedMousePosY;
    //this->pMouseDelta = state.rawMousePos;
    // proLog->print("input X: ", this->proPhysics.velocity.x);
}
