//           _______  _        _______  _______  _______  _______  _______     _        _______ _________
// |\     /|(  ____ \( (    /|(  ____ \(  ____ )(  ___  )(  ____ \(  ____ \   ( (    /|(  ____ \\__   __/
// ( \   / )| (    \/|  \  ( || (    \/| (    )|| (   ) || (    \/| (    \/   |  \  ( || (    \/   ) (
//  \ (_) / | (__    |   \ | || (_____ | (____)|| (___) || |      | (__       |   \ | || (__       | |
//   ) _ (  |  __)   | (\ \) |(_____  )|  _____)|  ___  || |      |  __)      | (\ \) ||  __)      | |
//  / ( ) \ | (      | | \   |      ) || (      | (   ) || |      | (         | | \   || (         | |
// ( /   \ )| (____/\| )  \  |/\____) || )      | )   ( || (____/\| (____/\ _ | )  \  || (____/\   | |
// |/     \|(_______/|/    )_)\_______)|/       |/     \|(_______/(_______/(_)|/    )_)(_______/   )_(
// Author: AlexHG @ xenspace.net
// License: MIT. Use at your own risk.

#include "gameObject.hpp"


GameObject::GameObject()
{
    this->proRenderHitbox = true;
    proSprite = new SGL_Sprite();
    proHitboxSprite = new SGL_Sprite();
}
GameObject::GameObject(SGL_Texture texture, SGL_Texture hitboxTexture)
{
    this->proSprite->texture = texture;
    this->proHitboxSprite->texture = hitboxTexture;

}
GameObject::~GameObject()
{
    delete proSprite;
    delete proHitboxSprite;
}

void GameObject::changeTexture(SGL_Texture texture, SGL_Texture hitboxTexture)
{
    this->proSprite->texture = texture;
    this->proHitboxSprite->texture =  hitboxTexture;
}
void GameObject::update(GLfloat deltaTime)
{

}
void GameObject::render()
{

}

void GameObject::init()
{

}


void GameObject::input(SGL_InputFrame &state)
{

}

glm::vec2 GameObject::position()
{
    return glm::vec2 (this->proSprite->position.x, this->proSprite->position.y);
}


glm::vec2 GameObject::center()
{
    return glm::vec2(this->proSprite->rotationOrigin.x + this->proSprite->position.x,
                     this->proSprite->rotationOrigin.y + this->proSprite->position.y);
}

SGL_Sprite &GameObject::getSprite()
{
    return *proSprite;
}

SGL_Sprite &GameObject::getHitboxSprite()
{
    return *proHitboxSprite;
}
