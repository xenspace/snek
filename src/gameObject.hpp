//           _______  _        _______  _______  _______  _______  _______     _        _______ _________
// |\     /|(  ____ \( (    /|(  ____ \(  ____ )(  ___  )(  ____ \(  ____ \   ( (    /|(  ____ \\__   __/
// ( \   / )| (    \/|  \  ( || (    \/| (    )|| (   ) || (    \/| (    \/   |  \  ( || (    \/   ) (
//  \ (_) / | (__    |   \ | || (_____ | (____)|| (___) || |      | (__       |   \ | || (__       | |
//   ) _ (  |  __)   | (\ \) |(_____  )|  _____)|  ___  || |      |  __)      | (\ \) ||  __)      | |
//  / ( ) \ | (      | | \   |      ) || (      | (   ) || |      | (         | | \   || (         | |
// ( /   \ )| (____/\| )  \  |/\____) || )      | )   ( || (____/\| (____/\ _ | )  \  || (____/\   | |
// |/     \|(_______/|/    )_)\_______)|/       |/     \|(_______/(_______/(_)|/    )_)(_______/   )_(
// Author: AlexHG @ xenspace.net
// License: MIT. Use at your own risk.

#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <GL/glew.h>
#include <iostream>
#include <string>
#include "skeletonGL/skeletonGL.hpp"



//entity hitbox for square collision detection
struct HitBox2D
{
    int x, y, w, h;
    bool colliding;
    SGL_Color color;
    HitBox2D() : x(0), y(0), w(0), h(0), colliding(false) {}
};


//basic physic properties for 2D objects
struct PhysicalProperties
{
    glm::vec2 acceleration, velocity;
};


class GameObject
{
protected:
    SGL_Sprite *proSprite, *proHitboxSprite;
    HitBox2D proHitbox;
    PhysicalProperties proPhysics;
    bool proRenderHitbox;

public:
    GameObject();
    GameObject(SGL_Texture texture, SGL_Texture hitboxTexture);

    virtual ~GameObject();

    void changeTexture(SGL_Texture texture, SGL_Texture hitboxTexture);

    virtual void init();
    virtual void update(GLfloat deltaTime);
    virtual void render();
    virtual void input(SGL_InputFrame &state);

    glm::vec2 position();
    glm::vec2 center();
    glm::vec2 size();
    glm::vec2 rotationOrigin();
    float rotation();

    SGL_Sprite &getSprite();
    SGL_Sprite &getHitboxSprite();
};


#endif
