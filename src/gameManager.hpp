//           _______  _        _______  _______  _______  _______  _______     _        _______ _________
// |\     /|(  ____ \( (    /|(  ____ \(  ____ )(  ___  )(  ____ \(  ____ \   ( (    /|(  ____ \\__   __/
// ( \   / )| (    \/|  \  ( || (    \/| (    )|| (   ) || (    \/| (    \/   |  \  ( || (    \/   ) (
//  \ (_) / | (__    |   \ | || (_____ | (____)|| (___) || |      | (__       |   \ | || (__       | |
//   ) _ (  |  __)   | (\ \) |(_____  )|  _____)|  ___  || |      |  __)      | (\ \) ||  __)      | |
//  / ( ) \ | (      | | \   |      ) || (      | (   ) || |      | (         | | \   || (         | |
// ( /   \ )| (____/\| )  \  |/\____) || )      | )   ( || (____/\| (____/\ _ | )  \  || (____/\   | |
// |/     \|(_______/|/    )_)\_______)|/       |/     \|(_______/(_______/(_)|/    )_)(_______/   )_(
// Author: AlexHG @ xenspace.net
// License: MIT. Use at your own risk.

#ifndef GAME_MANAGER_HPP
#define GAME_MANAGER_HPP


#include <iostream>
#include <sstream>
#include <ctime>
#include <vector>
#include <memory>
#include <stdio.h>
#include <thread>
#include <cstdint>
#include <random>

#include "gameObject.hpp"
#include "player.hpp"
#include "grid.hpp"

// All potential gamestates
enum class GAME_STATE : std::int8_t { START_SCREEN_MENU = 0, OPTIONS_MENU = 1, CREDITS_MENU = 2, IN_GAME = 3, INTRO_SEQUENCE = 4, GAME_COUNTDOWN = 5, GAME_OVER = 6, GAME_PAUSED = 7, END_ENUM = 8};
// Start menu options
enum class MENU_START_SCREEN_SELECTION : std::int8_t { START_GAME = 0, OPTIONS = 1, EXIT = 2, END_ENUM = 3};
// Settings menu options
enum class MENU_OPTIONS_SELECTION : std::int8_t { RESOLUTION = 0, DEBUG_PANEL = 1, APPLY = 2, END_ENUM = 3};
// Supported resolutions
enum class MENU_OPTIONS_RESOLUTION_SELECTION : std::int8_t { RES_360P = 0, RES_576P = 1, RES_720P = 2, RES_FULLSCREEN = 3, END_ENUM = 4};
// In game options
enum class MENU_IN_GAME_OPTIONS : std::uint8_t { TOGGLE_PAUSE = 0, BACK_TO_MAIN_MENU = 1, END_ENUM = 2 };

class GameManager
{
private:
    // Disable all copying and moving (singelton pattern)
    GameManager(const GameManager&) = delete;
    GameManager &operator = (const GameManager &) = delete;
    GameManager(GameManager &&) = delete;
    GameManager &operator = (GameManager &&) = delete;

    // SkeletonGL
    SGL_Window *pWindowManager;
    float pDeltaTime, pTimeCounter;
    // Cap delta time to a max value to stabilize physics in slower machines
    bool pDeltaTimeCap;
    SGL_InputFrame pDeltaInput;
    // Time counter testing
    float pIntroSequenceDuration, pIntroSequenceCounter;
    int pDeltaTimeCounter, pTotalSecondsPassed, pIntroDuration;

    std::vector<GameObject *> pGameObjects;
    std::vector<std::shared_ptr<SGL_Sprite>> pSprites;
    GAME_STATE pGameState;
    bool pMainLoopActive, pDrawDebugPanel;
    int8_t pMenuCursor;
    // Effects controllers
    GLfloat pShakeTime;
    float pCameraZoom;
    // Current settings
    MENU_OPTIONS_RESOLUTION_SELECTION pCurrentResolution;

    //grid test
    std::shared_ptr<Grid>pGameGrid;
    //required UI sprites
    std::shared_ptr<SGL_Sprite>pUISprite, pDebugPanelBackground, pIntroSequenceBG;

    Player *pPlayerOne;
public:

    GameManager();
    ~GameManager();

    int createSGLWindow();

    void mainLoop();
    void introSequence();
    void render(GAME_STATE gameState);
    void update(GAME_STATE gameState);
    void input(GAME_STATE gameState);

    //game assets loading, textures, shaders, fonts etc
    void loadAssets();
    //small window with program data for debbuging
    void drawDebugPanel(float x, float y, float fontSize, SGL_Color color);
    //renders all the menus
    void renderMainMenu();
    void renderOptionsMenu();
    void renderGameIntro();
    void renderGame(bool paused = false);
    void renderGameOver();
    void renderGameCountdown(int8_t seconds);
    //process the current cursor position
    void processMenu(GAME_STATE menu, int8_t cursor);

};



#endif
