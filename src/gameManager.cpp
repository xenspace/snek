//           _______  _        _______  _______  _______  _______  _______     _        _______ _________
// |\     /|(  ____ \( (    /|(  ____ \(  ____ )(  ___  )(  ____ \(  ____ \   ( (    /|(  ____ \\__   __/
// ( \   / )| (    \/|  \  ( || (    \/| (    )|| (   ) || (    \/| (    \/   |  \  ( || (    \/   ) (
//  \ (_) / | (__    |   \ | || (_____ | (____)|| (___) || |      | (__       |   \ | || (__       | |
//   ) _ (  |  __)   | (\ \) |(_____  )|  _____)|  ___  || |      |  __)      | (\ \) ||  __)      | |
//  / ( ) \ | (      | | \   |      ) || (      | (   ) || |      | (         | | \   || (         | |
// ( /   \ )| (____/\| )  \  |/\____) || )      | )   ( || (____/\| (____/\ _ | )  \  || (____/\   | |
// |/     \|(_______/|/    )_)\_______)|/       |/     \|(_______/(_______/(_)|/    )_)(_______/   )_(
// Author: AlexHG @ xenspace.net
// License: MIT. Use at your own risk.

#include "gameManager.hpp"

GameManager::GameManager()
{
    this->pWindowManager = new SGL_Window();
    //seed RNG
    srand(time(NULL));
    pDeltaTimeCap = false;
}

GameManager::~GameManager()
{
    delete pWindowManager;
}

int GameManager::createSGLWindow()
{
    SGL_Log("Creating SGL window...");
    try
    {
        this->pWindowManager->initializeWindow(0, 0, 1024, 576, 480, 272, std::string("SNEK"), false, 60, std::string("skeletongl.ini"));
    }
    catch (SGL_Exception &e)
    {
        SGL_Log("Aborting window creating procedure.");
        SGL_Log(e.what());
        assert(false);
        return -1;
    }
    SGL_Log("Window ready.");
    return 0;
}

void GameManager::mainLoop()
{
    SGL_Log("Loading the game...");
    pWindowManager->checkForErrors();
    this->loadAssets();
    pGameState = GAME_STATE::INTRO_SEQUENCE;
    pMainLoopActive = true;
    pDrawDebugPanel = false;
    pCameraZoom = 1.0f;
    pMenuCursor = 0;

    // Intro sequence
    pIntroSequenceDuration = (pWindowManager->getTickCount() / 1000);
    pIntroSequenceCounter = 0.0;

    // Get the current (external / window) resolution
    if (this->pWindowManager->getWindowCreationSpecs().fullScreen)
        pCurrentResolution = MENU_OPTIONS_RESOLUTION_SELECTION::RES_FULLSCREEN;
    else
    {
        switch (this->pWindowManager->getWindowCreationSpecs().currentH)
        {
        case 360:
            pCurrentResolution = MENU_OPTIONS_RESOLUTION_SELECTION::RES_360P; break;
        case 576:
            pCurrentResolution = MENU_OPTIONS_RESOLUTION_SELECTION::RES_576P; break;
        case 720:
            pCurrentResolution = MENU_OPTIONS_RESOLUTION_SELECTION::RES_720P; break;
        default:
        {
            SGL_Log("Requested height: " + std::to_string(this->pWindowManager->getWindowCreationSpecs().currentH));
            SGL_Log("Unsupported resolution selected, changing to 360P.");
            pCurrentResolution = MENU_OPTIONS_RESOLUTION_SELECTION::RES_360P;
            break;
        }

        }
    }
    WindowCreationSpecs windowSpecs = pWindowManager->getWindowCreationSpecs();
    // Load UI
    pUISprite = std::make_shared<SGL_Sprite>();
    pUISprite->shader = pWindowManager->assetManager->getShader("customSprite");
    pUISprite->position = glm::vec2(16, 8);
    pUISprite->size = glm::vec2(288, 64);
    pUISprite->color = SGL_Color(1.0f, 1.0f, 1.0f, 1.0f);
    pUISprite->texture = pWindowManager->assetManager->getTexture("UI");
    pUISprite->changeUVCoords(0, 32, 288, 96);
    // Intro sequence BG
    pIntroSequenceBG = std::make_shared<SGL_Sprite>();
    pIntroSequenceBG->position = glm::vec2(0, 0);
    pIntroSequenceBG->texture = pWindowManager->assetManager->getTexture("blankSquare");
    pIntroSequenceBG->size = glm::vec2(windowSpecs.internalW, (windowSpecs.internalH));
    pIntroSequenceBG->color = SGL_Color(0.0f, 0.0f, 0.0f, 1.0f);
    pIntroSequenceBG->blending = BLENDING_TYPE::DEFAULT_RENDERING;
    pIntroSequenceBG->shader = pWindowManager->assetManager->getShader("spriteUV");
    pIntroSequenceBG->resetUVCoords();

    // Debug panel background image
    pDebugPanelBackground = std::make_shared<SGL_Sprite>();
    pDebugPanelBackground->position = glm::vec2(0, 0);
    pDebugPanelBackground->texture = pWindowManager->assetManager->getTexture("blankSquare");
    pDebugPanelBackground->size = glm::vec2(windowSpecs.internalW, (windowSpecs.internalH));
    pDebugPanelBackground->color = SGL_Color(0.0f, 0.0f, 0.0f, 0.70f);
    pDebugPanelBackground->blending = BLENDING_TYPE::DEFAULT_RENDERING;
    pDebugPanelBackground->shader = pWindowManager->assetManager->getShader("spriteUV");
    pDebugPanelBackground->resetUVCoords();
    // Configure the player
    pPlayerOne = new Player();
    pGameObjects.push_back(pPlayerOne);
    pPlayerOne->init(pWindowManager->assetManager->getTexture("merchant"));

    //random number generator (thanks C++11)
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<> dis(0.0, 1.0);

    // Grid testing
    uint8_t tileSize = 16;
    pGameGrid = std::make_shared<Grid>(0, 0, (pWindowManager->getWindowCreationSpecs().internalW / tileSize), (pWindowManager->getWindowCreationSpecs().internalH / tileSize), tileSize, SGL_Color(1.0, 1.0, 1.0, 1.0), pWindowManager->assetManager->getTexture("blankSquare"));


    // Begin counting frame time for the intro sequence
    pIntroDuration = 3;
    pTotalSecondsPassed = 0;
    pDeltaTimeCounter = (pWindowManager->getTickCount() / 1000);
    // Must be called before entering the game loop to properly initialize
    // the post processor's FBO
    this->pWindowManager->setClearColor(SGL_Color(0.2f, 0.2f, 0.2f, 1.0f));
    while (pMainLoopActive)
    {
        // Must be called as soon as the frame starts to properly calculate delta time values and initiate the postprocessor FBO
        this->pWindowManager->startFrame();
        pDeltaTime = this->pWindowManager->getDeltaTime();

        this->pWindowManager->checkForErrors();

        GAME_STATE currentFrameState = pGameState; // Don't pass pGameState directly, it might change mid frame
        this->input(currentFrameState);
        this->update(currentFrameState);
        this->render(currentFrameState);

        this->pWindowManager->endFrame();
    }
    SGL_Log("Exiting main loop");
    for (auto it = pGameObjects.begin(); it != pGameObjects.end(); ++it)
    {
        delete (*it);
    }
    pGameObjects.clear();
}

void GameManager::update(GAME_STATE gameState)
{
    // ---------- //
    // DELTA TIME //
    // ---------- //
    float deltaTime = this->pWindowManager->getDeltaTime();
    float fixedDeltaTime = 0.016666;

    //update all of the game's entities
    for (auto iter = pGameObjects.begin(); iter != pGameObjects.end(); ++iter)
    {
        (*iter)->update(fixedDeltaTime);
    }

    if (gameState == GAME_STATE::IN_GAME)
    {
        //pGameGrid->update(pDeltaInput.normalizedMousePosX, pDeltaInput.normalizedMousePosY);
        if (!pGameGrid->update(fixedDeltaTime))
        {
            pGameState = GAME_STATE::GAME_OVER;
        }
    }

    // ------------- //
    // UPDATE CAMERA //
    // ------------- //
    //position the center of the camera on the center of the FBO texture (which has a size of internalW x interalH)
    this->pWindowManager->setCameraPosition(glm::vec2(pWindowManager->getWindowCreationSpecs().internalW / 2, pWindowManager->getWindowCreationSpecs().internalH / 2));
    // this->pWindowManager->cameraPosition(pPlayerOne->center());
    //zoom
    this->pWindowManager->setCameraScale(pCameraZoom);
    //update the internal offset matrix for rendering
    this->pWindowManager->updateCamera();

    // ---------------------- //
    // POSTPROCESSOR EFFECTS  //
    // ---------------------- //
    if (pShakeTime > 0.0f)
    {
        //this->pWindowManager->shakeScreen(true);
        this->pWindowManager->setChaosEffect(true);
        this->pWindowManager->setConfuseEffect(true);

        pShakeTime -= pDeltaTime;
        if (pShakeTime <= 0.0f)
        {
            this->pWindowManager->setShakeScreen(false);
            this->pWindowManager->setChaosEffect(false);
            this->pWindowManager->setConfuseEffect(false);
        }
    }
}


void GameManager::input(GAME_STATE gameState)
{
    SGL_InputFrame desiredKeys = this->pWindowManager->getFrameInput();

    switch (gameState)
    {
    case GAME_STATE::START_SCREEN_MENU:
    {
        if (desiredKeys.down && !pDeltaInput.down)
        {
            pMenuCursor++;
            if (pMenuCursor >= static_cast<int8_t>(MENU_START_SCREEN_SELECTION::END_ENUM))
                pMenuCursor = 0;

        }
        if (desiredKeys.up && !pDeltaInput.up)
        {
            pMenuCursor--;
            if (pMenuCursor < 0)
                pMenuCursor = (static_cast<int8_t>(MENU_START_SCREEN_SELECTION::END_ENUM) - 1);

        }
        if (desiredKeys.enter && !pDeltaInput.enter)
        {
            this->processMenu(GAME_STATE::START_SCREEN_MENU, pMenuCursor);
        }
        break;
    }
    case GAME_STATE::OPTIONS_MENU:
    {
        if (desiredKeys.down && !pDeltaInput.down)
        {
            pMenuCursor++;
            if (pMenuCursor >= static_cast<int8_t>(MENU_OPTIONS_SELECTION::END_ENUM))
                pMenuCursor = 0;

        }
        if (desiredKeys.up && !pDeltaInput.up)
        {
            pMenuCursor--;
            if (pMenuCursor < 0)
                pMenuCursor = (static_cast<int8_t>(MENU_OPTIONS_SELECTION::END_ENUM) - 1);

        }
        if (desiredKeys.enter && !pDeltaInput.enter)
        {
            this->processMenu(GAME_STATE::OPTIONS_MENU, pMenuCursor);
        }
        break;
    }
    case GAME_STATE::CREDITS_MENU:
    {
        if (desiredKeys.enter && !pDeltaInput.enter)
        {
            this->processMenu(GAME_STATE::CREDITS_MENU, 0);
        }
        break;
    }
    case GAME_STATE::IN_GAME:
    {
        if (desiredKeys.num1 && !pDeltaInput.num1)
            pGameState = GAME_STATE::GAME_PAUSED;


        if (desiredKeys.up && !pDeltaInput.up)
            pGameGrid->moveSnake(DIRECTION::UP);
        if (desiredKeys.down && !pDeltaInput.down)
            pGameGrid->moveSnake(DIRECTION::DOWN);
        if (desiredKeys.left && !pDeltaInput.left)
            pGameGrid->moveSnake(DIRECTION::LEFT);
        if (desiredKeys.right && !pDeltaInput.right)
            pGameGrid->moveSnake(DIRECTION::RIGHT);

        pPlayerOne->input(desiredKeys);
        break;
    }
    case GAME_STATE::GAME_PAUSED:
    {
        if (desiredKeys.num1 && !pDeltaInput.num1)
            pGameState = GAME_STATE::IN_GAME;
        if (desiredKeys.j && !pDeltaInput.j)
        {
            pGameGrid->resetGame();
            this->processMenu(GAME_STATE::IN_GAME, static_cast<uint8_t>(MENU_IN_GAME_OPTIONS::BACK_TO_MAIN_MENU));
        }
    }
    case GAME_STATE::GAME_OVER:
    {
        if (desiredKeys.space && !pDeltaInput.space)
        {
            pGameGrid->resetGame();
            this->processMenu(GAME_STATE::IN_GAME, static_cast<uint8_t>(MENU_IN_GAME_OPTIONS::BACK_TO_MAIN_MENU));
        }
    }
    } // switch end

    if (desiredKeys.esc || desiredKeys.sdlInternalQuit)
    {
        this->pMainLoopActive = false;
    }
    if (desiredKeys.c)
    {
        this->pShakeTime = 0.010f;
    }
    if (desiredKeys.p && !pDeltaInput.p)
    {
        this->pDrawDebugPanel = !pDrawDebugPanel;
    }
    if (desiredKeys.t)
    {

    }
    if (desiredKeys.up && !pDeltaInput.up)
    {

    }
    if (desiredKeys.down && !pDeltaInput.down)
    {

    }
    if (desiredKeys.enter && !pDeltaInput.enter)
    {

    }
    if (desiredKeys.left && !pDeltaInput.left)
    {

    }

    if (desiredKeys.right && !pDeltaInput.right)
    {

    }
    if (desiredKeys.n)
    {
        pWindowManager->resizeWindow(640, 360, true);
        pCurrentResolution = MENU_OPTIONS_RESOLUTION_SELECTION::RES_360P;
    }
    if (desiredKeys.b)
    {
        pWindowManager->resizeWindow(1024, 576, true);
        pCurrentResolution = MENU_OPTIONS_RESOLUTION_SELECTION::RES_576P;
    }
    if (desiredKeys.m)
    {
        pWindowManager->resizeWindow(1280, 720, true);
        pCurrentResolution = MENU_OPTIONS_RESOLUTION_SELECTION::RES_720P;
    }
    if (desiredKeys.f)
    {
        pWindowManager->toggleFullScreen(true);
    }
    if (desiredKeys.g)
    {
        pWindowManager->toggleFullScreen(false);
    }
    if (desiredKeys.e && !pDeltaInput.e)
    {
        pWindowManager->setPostProcessorShader(pWindowManager->assetManager->getShader("customPP"));
        SGL_Log("Custom PP shader.");
    }
    if (desiredKeys.r && !pDeltaInput.r)
    {
        pWindowManager->setPostProcessorShader(pWindowManager->assetManager->getShader("postProcessor"));
        SGL_Log("Default PP shader.");
    }
    if (desiredKeys.a)
        pCameraZoom += 0.010;
    if (desiredKeys.d)
        pCameraZoom -= 0.010;

    if (pCameraZoom < 0.0)
        pCameraZoom = 0.0;
    if (pCameraZoom > 2.0)
        pCameraZoom = 2.0;

    pDeltaInput = desiredKeys;
}

void GameManager::render(GAME_STATE gameState)
{
    // ----------- //
    // CAMERA MODE //
    // ----------- //
    this->pWindowManager->setCameraMode(CAMERA_MODE::DEFAULT);

    switch (gameState)
    {
    case GAME_STATE::START_SCREEN_MENU:
    {
        renderMainMenu();
        break;
    }
    case GAME_STATE::OPTIONS_MENU:
    {
        renderOptionsMenu();
        break;
    }
    case GAME_STATE::IN_GAME:
    {
        renderGame();
        break;
    }
    case GAME_STATE::GAME_PAUSED:
    {
        renderGame(true);
        break;
    }

    case GAME_STATE::INTRO_SEQUENCE:
    {
        renderGameIntro();
        break;
    }
    case GAME_STATE::GAME_OVER:
    {
        renderGameOver();
        break;
    }
    case GAME_STATE::GAME_COUNTDOWN:
    {
        renderGameCountdown(3);
        break;
    }
    }

    this->pWindowManager->setCameraMode(CAMERA_MODE::OVERLAY);
    if (pDrawDebugPanel)
        this->drawDebugPanel(16.0f, 16.0f, 0.16, SGL_Color(1.0f,1.0f,1.0f,1.0f));

    // END RENDER
    this->pWindowManager->setCameraMode(CAMERA_MODE::DEFAULT);
}

void GameManager::renderGameIntro()
{
    // Render Background image
    this->pWindowManager->renderer->renderSprite(*pIntroSequenceBG);

    int center = (pWindowManager->getWindowCreationSpecs().internalW / 2) - (45 * 4);

    pWindowManager->renderer->renderText("powered by SkeletonGL", center, 80, 0.58, SGL_Color(1.0f,1.0f,1.0f,1.0f));
    pWindowManager->renderer->renderText("visit xenid.net for more", center, 110, 0.58, SGL_Color(1.0f,1.0f,1.0f,1.0f));
    pIntroSequenceCounter += (pWindowManager->getTickCount() / 1000);
    // Get time since startup
    int secondsSinceStart = (pWindowManager->getTickCount() / 1000);
    if (secondsSinceStart != pDeltaTimeCounter)
    {
        pTotalSecondsPassed++;
        pDeltaTimeCounter = secondsSinceStart;
    }
    if (pTotalSecondsPassed >= pIntroDuration) // Starts counting at 0 (desired time - 1)
    {
        SGL_Log("-- Sequence duration: " + std::to_string(pTotalSecondsPassed));
        SGL_Log("-- Sequence counter: " + std::to_string(pDeltaTimeCounter));
        pTotalSecondsPassed = 0;
        pGameState = GAME_STATE::START_SCREEN_MENU;
    }

}

void GameManager::renderGameOver()
{
    // Render Background image
    this->pWindowManager->renderer->renderSprite(*pIntroSequenceBG);
    int center = (pWindowManager->getWindowCreationSpecs().internalW / 2) - (110);
    pWindowManager->renderer->renderText("GAME OVER", center, 80, 0.80, SGL_Color(1.0f,1.0f,1.0f,1.0f));
    pWindowManager->renderer->renderText("score: " + std::to_string(pGameGrid->getScore()), center, 130, 0.32, SGL_Color(1.0f,1.0f,1.0f,1.0f));
    pWindowManager->renderer->renderText("press space to continue", center, 180, 0.32, SGL_Color(1.0f,1.0f,1.0f,1.0f));
}


void GameManager::renderGameCountdown(int8_t seconds)
{
    // Render Background image
    this->pWindowManager->renderer->renderSprite(*pIntroSequenceBG);

    int center = (pWindowManager->getWindowCreationSpecs().internalW / 2) - (45 * 4);

    pWindowManager->renderer->renderText("powered by SkeletonGL", center, 80, 0.58, SGL_Color(1.0f,1.0f,1.0f,1.0f));
    pIntroSequenceCounter += (pWindowManager->getTickCount() / 1000);
    SGL_Log("-- Sequence duration: " + std::to_string(pTotalSecondsPassed));
    SGL_Log("-- Sequence counter: " + std::to_string(pDeltaTimeCounter));

    // Get time since startup
    int secondsSinceStart = (pWindowManager->getTickCount() / 1000);
    if (secondsSinceStart != pDeltaTimeCounter)
    {
        pTotalSecondsPassed++;
        pDeltaTimeCounter = secondsSinceStart;
    }
    if (pTotalSecondsPassed >= pIntroDuration) // Starts counting at 0 (desired time - 1)
    {
        SGL_Log("-- Sequence duration: " + std::to_string(pTotalSecondsPassed));
        SGL_Log("-- Sequence counter: " + std::to_string(pDeltaTimeCounter));
        pTotalSecondsPassed = 0;
        pGameState = GAME_STATE::START_SCREEN_MENU;
    }

}

void GameManager::renderGame(bool paused)
{
    SGL_Color selectedColor = {1.0, 1.0, 1.0, 1.0};

    pGameGrid->render(*pWindowManager->renderer.get(), *pWindowManager->assetManager.get());
    if (paused)
    {
        pWindowManager->renderer->renderText("PAUSED", 40, 80, 0.58, SGL_Color(1.0f,1.0f,1.0f,1.0f));
        pWindowManager->renderer->renderText("press 1 to unpause", 40, 110, 0.32, SGL_Color(1.0f,1.0f,1.0f,1.0f));
        pWindowManager->renderer->renderText("press 'j' to return to the main menu", 30, 160, 0.32, SGL_Color(1.0f,1.0f,1.0f,1.0f));
    }

}

//MENUS
void GameManager::renderMainMenu()
{
    // Title
    pUISprite->position = glm::vec2(40, 8);
    pUISprite->size = glm::vec2(400, 64);
    pUISprite->color = SGL_Color(1.0f, 1.0f, 1.0f, 1.0f);
    pUISprite->changeUVCoords(0, 0, 400, 112);
    this->pWindowManager->renderer->renderSprite((*pUISprite));
    std::int16_t menuOffsetX = 80;
    std::int16_t menuOffsetY = 84;
    std::int16_t textHeight = 32;
    // Start
    pUISprite->position = glm::vec2(menuOffsetX, menuOffsetY);
    pUISprite->size = glm::vec2(96, 32);
    pUISprite->color = SGL_Color(1.0f, 1.0f, 1.0f, 1.0f);
    pUISprite->changeUVCoords(0, 112, 96, 32);
    this->pWindowManager->renderer->renderSprite((*pUISprite));
    // Options
    pUISprite->position = glm::vec2(menuOffsetX, menuOffsetY + textHeight);
    pUISprite->size = glm::vec2(144, 32);
    pUISprite->color = SGL_Color(1.0f, 1.0f, 1.0f, 1.0f);
    pUISprite->changeUVCoords(0, 144, 144, 32);
    this->pWindowManager->renderer->renderSprite((*pUISprite));
    // Exit
    pUISprite->position = glm::vec2(menuOffsetX,  menuOffsetY + textHeight * 2);
    pUISprite->size = glm::vec2(80, 32);
    pUISprite->color = SGL_Color(1.0f, 1.0f, 1.0f, 1.0f);
    pUISprite->changeUVCoords(0, 176, 80, 32);
    this->pWindowManager->renderer->renderSprite((*pUISprite));
    // Cursor
    pUISprite->position = glm::vec2(menuOffsetX - 28, (menuOffsetY + 8) + (pMenuCursor * textHeight ));
    pUISprite->size = glm::vec2(16, 16);
    pUISprite->color = SGL_Color(1.0f, 1.0f, 1.0f, 1.0f);
    pUISprite->changeUVCoords(368, 144, 32, 32);
    this->pWindowManager->renderer->renderSprite((*pUISprite));
}


void GameManager::renderOptionsMenu()
{
    // Title
    pUISprite->position = glm::vec2(40, 8);
    pUISprite->size = glm::vec2(400, 64);
    pUISprite->color = SGL_Color(1.0f, 1.0f, 1.0f, 1.0f);
    pUISprite->changeUVCoords(0, 0, 400, 112);
    this->pWindowManager->renderer->renderSprite((*pUISprite));

    std::int16_t menuOffsetX = 80;
    std::int16_t menuOffsetY = 84;
    std::int16_t textHeight = 32;
    SGL_Color settingColor = {0.90, 0.3, 0.20, 1.0};
    SGL_Color selectionColor = {0.30, 0.3, 0.90, 1.0};
    SGL_Color cursorColor = {0.80, 0.8, 0.90, 1.0};
    // Resolution
    pUISprite->position = glm::vec2(menuOffsetX, menuOffsetY);
    pUISprite->size = glm::vec2(240, 32);
    pUISprite->changeUVCoords(0, 240, 208, 32);
    pUISprite->color = settingColor;
    this->pWindowManager->renderer->renderSprite((*pUISprite));

    // Debug panel
    pUISprite->position = glm::vec2(menuOffsetX, (menuOffsetY + (textHeight * 2)) );
    pUISprite->size = glm::vec2(272, 32);
    pUISprite->changeUVCoords(0, 272, 224, 32);
    pUISprite->color = settingColor;
    this->pWindowManager->renderer->renderSprite((*pUISprite));

    // Back
    pUISprite->position = glm::vec2(menuOffsetX, (menuOffsetY + (textHeight * 4)) );
    pUISprite->size = glm::vec2(80, 32);
    pUISprite->changeUVCoords(0, 208, 80, 32);
    pUISprite->color = settingColor;
    this->pWindowManager->renderer->renderSprite((*pUISprite));


    if (pDrawDebugPanel)
    {
        // ON
        pUISprite->position = glm::vec2(menuOffsetX, (menuOffsetY + (textHeight * 3)) );
        pUISprite->size = glm::vec2(32, 32);
        pUISprite->changeUVCoords(0, 432, 48, 32);
        pUISprite->color = selectionColor;
        this->pWindowManager->renderer->renderSprite((*pUISprite));
    }
    else
    {
        // OFF
        pUISprite->position = glm::vec2(menuOffsetX, (menuOffsetY + (textHeight * 3)) );
        pUISprite->size = glm::vec2(48, 32);
        pUISprite->changeUVCoords(48, 432, 64, 32);
        pUISprite->color = selectionColor;
        this->pWindowManager->renderer->renderSprite((*pUISprite));
    }

    switch (pCurrentResolution)
    {
    case MENU_OPTIONS_RESOLUTION_SELECTION::RES_360P:
    {
        // 640 x 320
        pUISprite->position = glm::vec2(menuOffsetX, menuOffsetY + textHeight);
        pUISprite->size = glm::vec2(144, 32);
        pUISprite->changeUVCoords(0, 304, 144, 32);
        pUISprite->color = selectionColor;
        break;
    }
    case MENU_OPTIONS_RESOLUTION_SELECTION::RES_576P:
    {
        // 1024 x 576
        pUISprite->position = glm::vec2(menuOffsetX, menuOffsetY + textHeight);
        pUISprite->size = glm::vec2(160, 32);
        pUISprite->changeUVCoords(0, 336, 160, 32);
        pUISprite->color = selectionColor;
        break;
    }
    case MENU_OPTIONS_RESOLUTION_SELECTION::RES_720P:
    {
        // 1280 x 720 (HD)
        pUISprite->position = glm::vec2(menuOffsetX, menuOffsetY + textHeight);
        pUISprite->size = glm::vec2(160, 32);
        pUISprite->changeUVCoords(0, 368, 160, 32);
        pUISprite->color = selectionColor;
        break;
    }
    case MENU_OPTIONS_RESOLUTION_SELECTION::RES_FULLSCREEN:
    {
        // FULLSCREEN
        pUISprite->position = glm::vec2(menuOffsetX, menuOffsetY + textHeight);
        pUISprite->size = glm::vec2(208, 32);
        pUISprite->changeUVCoords(0, 400, 208, 32);
        pUISprite->color = selectionColor;
        break;
    }
    }
    this->pWindowManager->renderer->renderSprite((*pUISprite));

    // Cursor
    pUISprite->position = glm::vec2(menuOffsetX - 28, (menuOffsetY + 8) + (pMenuCursor * (textHeight *2) ));
    pUISprite->size = glm::vec2(16, 16);
    pUISprite->color = cursorColor;
    pUISprite->changeUVCoords(368, 144, 32, 32);
    this->pWindowManager->renderer->renderSprite((*pUISprite));
}


//small window with program data for debbuging
void GameManager::drawDebugPanel(float x, float y, float fontSize, SGL_Color color)
{
    float offset = (fontSize * 100) / 2;
    SGL_Color titleColor = {1.0f, 0.30f, 0.30f, 1.0f};
    SGL_Color subTitleColor = {0.30f, 1.0f, 0.30f, 1.0f};
    int center = (pWindowManager->getWindowCreationSpecs().internalW / 2) - (x * 4);
    int left = x;
    int right = (pWindowManager->getWindowCreationSpecs().internalW) - (x * 8);
    int spacing = offset * 6;
    WindowCreationSpecs windowSpecs = pWindowManager->getWindowCreationSpecs();
    std::stringstream textureMem;

    // Background image
    this->pWindowManager->renderer->renderSprite((*pDebugPanelBackground));

    std::string windowFocus = "Has focus: ";
    windowFocus += (pWindowManager->hasKeyboardFocus() ? "YES" : "NO");
    std::string isFullscreen = "Fullscreen: ";
    isFullscreen += (windowSpecs.fullScreen ? "YES" : "NO");
    std::string VSYNC = "VSYNC: ";
    VSYNC += (windowSpecs.activeVSYNC ? "YES" : "NO");
    std::string LMB = "LMB: ";
    LMB += (pDeltaInput.mouseLeft ? "PRESSED" : "-----");
    textureMem << "GPU Texture Memory (MB): " << (static_cast<float>(pWindowManager->assetManager->getTextureMemoryGPU()) / 1024) / 1024;

    // Title
    pWindowManager->renderer->renderText("SkeletonGL Debug Panel", center, y, fontSize, titleColor);
    pWindowManager->renderer->renderText("Ver 0.8.0", right, y, fontSize, titleColor);

    // Engine state block (top left)
    pWindowManager->renderer->renderText("--- ENGINE STATUS ---", left, y + (offset * 1.0f), fontSize, subTitleColor);
    pWindowManager->renderer->renderText("Host OS: " + pWindowManager->hostData->hostOS, left, y + (offset * 2.0f), fontSize, color);
    pWindowManager->renderer->renderText("Delta time: " + std::to_string(pDeltaTime), left, y + (offset * 3.0f), fontSize, color);
    pWindowManager->renderer->renderText("FPS: " + std::to_string(static_cast<int>(1.0 / pDeltaTime)), left, y + (offset * 4.0f), fontSize, color);
    pWindowManager->renderer->renderText("Time elapsed: " + std::to_string((pWindowManager->getTickCount() / 1000)), left, y + (offset * 5.0f), fontSize, color);
    pWindowManager->renderer->renderText(textureMem.str(), left, y + (offset * 6.0f), fontSize, color);

    pWindowManager->renderer->renderText("--- CAMERA ---", left, y + (offset * 7.0f), fontSize, subTitleColor);
    pWindowManager->renderer->renderText("X: " + std::to_string(pWindowManager->getCameraPosX()), left, y + (offset * 8.0f), fontSize, color);
    pWindowManager->renderer->renderText("Y: " + std::to_string(pWindowManager->getCameraPosY()), left + spacing, y + (offset * 8.0f), fontSize, color);
    pWindowManager->renderer->renderText("W: " + std::to_string(pWindowManager->getCameraPosW()), left, y + (offset * 9.0f), fontSize, color);
    pWindowManager->renderer->renderText("H: " + std::to_string(pWindowManager->getCameraPosH()), left + spacing, y + (offset * 9.0f), fontSize, color);
    pWindowManager->renderer->renderText("Zoom: " + std::to_string(pCameraZoom), left, y + (offset * 10.0f), fontSize, color);

    pWindowManager->renderer->renderText("--- CONSOLE ---", left, y + (offset * 12.0f), fontSize, subTitleColor);
    for (int i = 16; i >= 1; --i)
    {
        pWindowManager->renderer->renderText(SGL_LOG_HISTORY[SGL_LOG_HISTORY.size() - i], left, y + (offset * (29.0f - i)), fontSize, color);
    }

    // Center
    pWindowManager->renderer->renderText("--- MAIN WINDOW ---", center, y + (offset * 1.0f), fontSize, subTitleColor);
    pWindowManager->renderer->renderText("Display resolution: " + std::to_string(pWindowManager->getWindowCreationSpecs().currentW) + " x " +
                                         std::to_string(pWindowManager->getWindowCreationSpecs().currentH),
                                         center, y + (offset * 2.0f), fontSize, color);
    pWindowManager->renderer->renderText("Internal resolution: " + std::to_string(pWindowManager->getWindowCreationSpecs().internalW) + " x " +
                                         std::to_string(pWindowManager->getWindowCreationSpecs().internalH),
                                         center, y + (offset * 3.0f), fontSize, color);
    pWindowManager->renderer->renderText(windowFocus, center, y + (offset * 4.0f), fontSize, color);
    pWindowManager->renderer->renderText(isFullscreen, center, y + (offset * 5.0f), fontSize, color);
    pWindowManager->renderer->renderText(VSYNC, center, y + (offset * 6.0f), fontSize, color);


    // Right
    pWindowManager->renderer->renderText("--- MOUSE ---", right, y + (offset * 1.0f), fontSize, subTitleColor);
    pWindowManager->renderer->renderText("X: " + std::to_string(pDeltaInput.rawMousePosX),
                                         right,  y + (offset * 2.0f), fontSize, color);
    pWindowManager->renderer->renderText("N: " + std::to_string(pDeltaInput.normalizedMousePosX),
                                         right + spacing,  y + (offset * 2.0f), fontSize, color);
    pWindowManager->renderer->renderText("Y: " + std::to_string(pDeltaInput.rawMousePosY),
                                         right,  y + (offset * 3.0f), fontSize, color);
    pWindowManager->renderer->renderText("N: " + std::to_string(pDeltaInput.normalizedMousePosY),
                                         right + spacing,  y + (offset * 3.0f), fontSize, color);
}

void GameManager::processMenu(GAME_STATE menu, int8_t cursor)
{
    if (menu == GAME_STATE::START_SCREEN_MENU)
    {
        MENU_START_SCREEN_SELECTION optionSelected = static_cast<MENU_START_SCREEN_SELECTION>(cursor);
        switch (optionSelected)
        {
        case MENU_START_SCREEN_SELECTION::START_GAME:
        {
            SGL_Log("START GAME PRESSED");
            pGameState = GAME_STATE::IN_GAME;
            pMenuCursor = 0;
            break;
        }
        case MENU_START_SCREEN_SELECTION::OPTIONS:
        {
            SGL_Log("OPTIONS PRESSED");
            pGameState = GAME_STATE::OPTIONS_MENU;
            pMenuCursor = 0;
            break;
        }
        case MENU_START_SCREEN_SELECTION::EXIT:
        {
            SGL_Log("EXIT PRESSED");
            pMenuCursor = 0;
            pMainLoopActive = false;
            break;
        }
        default:
        {
            SGL_Log("Processing out of bounds menu cursor. Exiting.");
            pMainLoopActive = false;
            break;
        }
        }
    }
    else if (menu == GAME_STATE::OPTIONS_MENU)
    {
        MENU_OPTIONS_SELECTION optionSelected = static_cast<MENU_OPTIONS_SELECTION>(cursor);
        switch (optionSelected)
        {
        case MENU_OPTIONS_SELECTION::RESOLUTION:
        {
            if ((static_cast<uint8_t>(pCurrentResolution) + 1) >= (static_cast<uint8_t>(MENU_OPTIONS_RESOLUTION_SELECTION::END_ENUM)))
                pCurrentResolution = static_cast<MENU_OPTIONS_RESOLUTION_SELECTION>(0);
            else
                pCurrentResolution = static_cast<MENU_OPTIONS_RESOLUTION_SELECTION>(static_cast<uint8_t>(pCurrentResolution) + 1);

            if (pCurrentResolution == MENU_OPTIONS_RESOLUTION_SELECTION::RES_FULLSCREEN)
                pWindowManager->toggleFullScreen(true);
            else if (pCurrentResolution == MENU_OPTIONS_RESOLUTION_SELECTION::RES_360P)
            {
                pWindowManager->toggleFullScreen(false);
                pWindowManager->resizeWindow(640, 360, true);
            }
            else if (pCurrentResolution == MENU_OPTIONS_RESOLUTION_SELECTION::RES_576P)
            {
                pWindowManager->toggleFullScreen(false);
                pWindowManager->resizeWindow(1024, 576, true);
            }
            else if (pCurrentResolution == MENU_OPTIONS_RESOLUTION_SELECTION::RES_720P)
            {
                pWindowManager->toggleFullScreen(false);
                pWindowManager->resizeWindow(1280, 720, true);
            }


            SGL_Log("RESOLUTION PRESSED: " + std::to_string(static_cast<uint8_t>(pCurrentResolution)));
            break;
        }
        case MENU_OPTIONS_SELECTION::DEBUG_PANEL:
        {
            SGL_Log("DEBUG PANEL PRESSED");
            pDrawDebugPanel = !pDrawDebugPanel;
            break;
        }
        case MENU_OPTIONS_SELECTION::APPLY:
        {
            SGL_Log("APPLY PRESSED");
            //reset the menu cursor
            pMenuCursor = 0;
            pGameState = GAME_STATE::START_SCREEN_MENU;
            break;
        }
        default:
        {
            SGL_Log("Processing out of bounds menu cursor. Exiting.");
            pMainLoopActive = false;
            break;
        }
        }
    } // end options
    else if (menu == GAME_STATE::IN_GAME)
    {
        MENU_IN_GAME_OPTIONS optionSelected = static_cast<MENU_IN_GAME_OPTIONS>(cursor);
        switch (optionSelected)
        {
        case MENU_IN_GAME_OPTIONS::TOGGLE_PAUSE:
        {

            break;
        }
        case MENU_IN_GAME_OPTIONS::BACK_TO_MAIN_MENU:
        {
            SGL_Log("Going back to main menu");
            pGameState = GAME_STATE::START_SCREEN_MENU;
            break;
        }
        default:
        {
            SGL_Log("Processing out of bounds menu cursor. Exiting.");
            pMainLoopActive = false;
            break;
        }

        }
    }
}


//game assets loading, textures, shaders, fonts etc
void GameManager::loadAssets()
{
    SGL_Log("Loading assets....");
    pWindowManager->checkForErrors();
    SGL_Log("Loading 2D textures.");

    pWindowManager->assetManager->loadTexture("assets/textures/snake_ui.png", true, "UI");
    SGL_Log("Loading custom shaders.");
    pWindowManager->assetManager->loadShaders("assets/shaders/customSpriteV.c", "assets/shaders/customSpriteF.c", nullptr, "customSprite", SHADER_TYPE::SPRITE);
    pWindowManager->assetManager->loadShaders("assets/shaders/customLineV.c", "assets/shaders/customLineF.c",nullptr, "customLine", SHADER_TYPE::LINE);
    pWindowManager->assetManager->loadShaders("assets/shaders/customPointV.c", "assets/shaders/customPointF.c",nullptr, "customPoint", SHADER_TYPE::PIXEL);
    pWindowManager->assetManager->loadShaders("assets/shaders/customTextV.c", "assets/shaders/customTextF.c",nullptr, "customText", SHADER_TYPE::TEXT);
    pWindowManager->assetManager->loadShaders("assets/shaders/customFBOV.c", "assets/shaders/customFBOF.c",nullptr, "customPP", SHADER_TYPE::POST_PROCESSOR);

    pWindowManager->checkForErrors();
    SGL_Log("Exiting asset loading.");
}
