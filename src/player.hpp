//           _______  _        _______  _______  _______  _______  _______     _        _______ _________
// |\     /|(  ____ \( (    /|(  ____ \(  ____ )(  ___  )(  ____ \(  ____ \   ( (    /|(  ____ \\__   __/
// ( \   / )| (    \/|  \  ( || (    \/| (    )|| (   ) || (    \/| (    \/   |  \  ( || (    \/   ) (
//  \ (_) / | (__    |   \ | || (_____ | (____)|| (___) || |      | (__       |   \ | || (__       | |
//   ) _ (  |  __)   | (\ \) |(_____  )|  _____)|  ___  || |      |  __)      | (\ \) ||  __)      | |
//  / ( ) \ | (      | | \   |      ) || (      | (   ) || |      | (         | | \   || (         | |
// ( /   \ )| (____/\| )  \  |/\____) || )      | )   ( || (____/\| (____/\ _ | )  \  || (____/\   | |
// |/     \|(_______/|/    )_)\_______)|/       |/     \|(_______/(_______/(_)|/    )_)(_______/   )_(
// Author: AlexHG @ xenspace.net
// License: MIT. Use at your own risk.

#ifndef PLAYER_H
#define PLAYER_H

#include <GL/glew.h>
#include <iostream>
#include <string>

#include "skeletonGL/utility/SGL_DataStructures.hpp"
#include "gameObject.hpp"
#include "skeletonGL/skeletonGL.hpp"



class Player : public GameObject
{
private:
    bool renderHitbox;
    float rot;
    int pMouseDeltaX, pMouseDeltaY;
    // ParticleGenerator *pParticles;
public:
    Player();
    ~Player();
    //virtual
    void init(SGL_Texture texture);
    void update(GLfloat deltaTime);
    void input(SGL_InputFrame &state);
    void render();
};


#endif
